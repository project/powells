<?php

/**
 * Implementation of hook_help().
 */
function powells_help($section) {
	if ($section == 'admin/settings/powells') {
		return t('Powell\'s settings help.');
	}
}

/**
 * Implementation of hook_node_types().
 */
function powells_node_types() {
	return array('powells-book');
}

/**
 * Implementation of hook_node_name().
 */
function powells_node_name($node) {
	switch (is_string($node) ? $node : $node->type) {
		case 'powells-book':
			return t('powell\'s book');
	}
}

/**
 * Implementation of hook_access().
 */
function powells_access($op, $node) {
  global $user;

  if ($op == 'create') {
    return user_access('create powells book');
  }

  if ($op == 'update' || $op == 'delete') {
    if (user_access('edit own powells book') && ($user->uid == $node->uid)) {
      return TRUE;
    }
  }
}

/**
 * Implementation of hook_perm().
 */
function powells_perm() {
  return array('create powells book', 'edit own powells book');
}

/**
 * Implementation of hook_link().
 */
function powells_link($type, $node = 0, $main) {
  $links = array();

  if ($type == 'node' && $node->type == 'powells-book') {
		$partner = variable_get('powells_partner', '29362');
		$link_url = 'http://www.powells.com/cgi-bin/partner?partner_id='.$partner.'&cgi=product&isbn='.$node->isbn;
    $links[] = l(t('buy this book'), $link_url);
    if (powells_access('update', $node) && !user_access('administer nodes')) {
      $links[] = l(t('edit this book'), "node/$node->nid/edit");
    }
  }

  return $links;
}

/**
 * Implementation of hook_menu().
 */
function powells_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
			'path' => 'node/add/powells_book',
			'title' => t('powell\'s book'),
      'access' => user_access('create powells book')
		);
		$items[] = array(
			'path'     => 'powells/books',
			'title'    => variable_get('powells_page_title', 'powell\'s books'),
			'callback' => 'powells_books_page',
			'access'   => TRUE,
			'type'     => MENU_CALLBACK
		);
  }

  return $items;
}

/**
 * Implementation of hook_form().
 */
function powells_form(&$node, &$error) {
  $output = '';

	if ($node->type == 'powells-book') {
	  if (function_exists('taxonomy_node_form')) {
  	  $output .= implode('', taxonomy_node_form('powells-book', $node));
	  }
	  $output .= form_textfield(t('ISBN'), 'isbn', $node->isbn, 15, 10);
  	$output .= form_select(t('Weight'), 'weight', $node->weight, array(
			'-10'=>-10, '-9'=>-9, '-8'=>-8, '-7'=>-7, '-6'=>-6, '-5'=>-5, '-4'=>-4, '-3'=>-3, '-2'=>-2, '-1'=>-1,
			'0'=>0,
			'1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9, '10'=>10
		));
		$output .= form_textarea(t('Description'), 'body', $node->body, 60, 20, '', NULL, TRUE);
		$output .= filter_form('format', $node->format);
	}
	
  return $output;
}

/**
 * Implementation of hook_validate().
 */
function powells_validate(&$node) {
	// Add validation code.  :)
}

/**
 * Implementation of hook_insert().
 */
function powells_insert($node) {
	if ($node->type == 'powells-book') {
	  db_query("INSERT INTO {powells_books} (nid, isbn, weight) VALUES (%d, '%s', %d)", $node->nid, $node->isbn, $node->weight);
	}
}

/**
 * Implementation of hook_update().
 */
function powells_update($node) {
	if ($node->type == 'powells-book') {
  	db_query("UPDATE {powells_books} SET isbn = '%s', weight = %d WHERE nid = %d", $node->isbn, $node->weight, $node->nid);
	}
}

/**
 * Implementation of hook_delete().
 */
function powells_delete($node) {
	if ($node->type == 'powells-book') {
  	db_query('DELETE FROM {powells_books} WHERE nid = %d', $node->nid);
	}
}

/**
 * Implementation of hook_load().
 */
function powells_load($node) {
	if ($node->type == 'powells-book') {
  	$obj = db_fetch_object(db_query('SELECT isbn,weight FROM {powells_books} WHERE nid = %d', $node->nid));
	}
  return $obj;
}

/**
 * Implementation of hook_view().
 */
function powells_view(&$node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
	if ($node->type == 'powells-book') {
		$node->body = theme('powells_book', $node);
		$node->teaser = $node->body;
	}
}

/**
 * A custom theme function.
 */
function theme_powells_book($node) {
	$partner = variable_get('powells_partner', '29362');
	$link_url = 'http://www.powells.com/cgi-bin/partner?partner_id='.$partner.'&cgi=product&isbn='.$node->isbn;
	$image_url = 'http://content.powells.com/cgi-bin/imageDB.cgi?isbn='.$node->isbn.'&t=1';
	$output = 
		'<div class="powells_book">';
	if (variable_get('powells_show_book_image','1')=='1') {
		$output .=
			'<a href="'.$link_url.'"><img src="'.$image_url.'" align="left"></a>';
	}
	$output .=
		$node->body.' [<a href="'.$link_url.'">read more</a>]'.
		'</div>';
	return $output;
}

/**
 * Page to view books.
 */
function powells_books_page () {
	$partner = variable_get('powells_partner', '29362');
	$content = '';
	if (variable_get('powells_show_search','1')=='1') {
		$content .= 
			'<form action="http://www.powells.com/cgi-bin/partner" method="post">'.
			'<input type="hidden" value="'.$partner.'" name="partner_id"/>'.
			'<input type="hidden" name="cgi" value="search/search"/>'.
			'<input type="hidden" name="searchtype" value="kw"/>'.
			'<table class="powells_book_search" cellspacing="0"><tr>'.
				'<td>Search for a Book:</td>'.
				'<td><input value="" name="searchfor" size="12"/></td>'.
			  '<td><input type="submit" value="Search"/></td>'.
			'</tr></table>'.
			'</form>';
	}
	$result = db_query('
		SELECT n.nid 
		FROM {powells_books} as pb,{node} as n 
		WHERE pb.nid=n.nid 
		ORDER BY pb.weight, n.title
	');
	while ($node = db_fetch_object($result)) {
		$node = node_load(array('nid'=>$node->nid));
		$content .= node_view($node,TRUE);
	}
	print theme('page', $content);
}

/**
 * Implementation of hook_powells().
 */
function powells_settings() {
	$output = 
		form_textfield(
			t('Partner ID'),
			'powells_partner',
			variable_get('powells_partner', '29362'),
			10, 8,
			t('Your Powell\'s partner ID.')
		).

		form_group('Books',
		
		form_textfield(
			t('Page Title'),
			'powells_page_title',
			variable_get('powells_page_title', 'powell\'s books'),
			32, 64,
			t('The title of the powell\'s book page.')
		).
		
		form_checkbox(
			t('Show search box'),
			'powells_show_search',
			1,
			(variable_get('powells_show_search','1')=='1'?1:0),
			t('Whether or not to display the powell\'s books search form.')
		).
		
		form_checkbox(
			t('Show book image'),
			'powells_show_book_image',
			1,
			(variable_get('powells_show_book_image','1')=='1'?1:0),
			t('Whether or not to display a thumbnail image of books.')
		)

		,'The books page is viewed at the url powells/books.');
	return $output;
}

?>
