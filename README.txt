This module adds a node called "powell's book" which is fairly basic 
book node with a title, description, isbn, and a listing weight.

Books may be configured to show a thumbnail image of themselves.  A 
page of books is created at the powells/books path.  Links to the 
powell's web site are generated useing your powell's Parnter ID.

Powell's is an online bookstore with a vast inventory and impressive
partner program.  Visit them at www.powells.com.

Send comments to aran@electroniclife.org

Note: Tables still need to be written and SQL needs to be 
tested on other databases.  Until that happens helpedit only 
support MySQL.

Installation
--------
- Copy powells.module to your modules directory.
- Run the SQL in powells.mysql.
- Go to administer->modules and enable the powells module.
- Go to administer->settings->powells and adjust any settings.

Usage
--------
- Create "powell's book" nodes.
- View books at powells/books.

